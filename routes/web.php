<?php


Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();
Route::get('logout', 'UserController@logout');

Route::get('/home', 'HomeController@index');


Route::group(['middleware' => ['web','auth']], function () {

    Route::get('customer/create', 'CustomerController@createCustomer');
    Route::post('customer/create', 'CustomerController@storeCustomer');
    Route::get('customer/list', 'CustomerController@listCustomers');
    Route::get('customer/{id}/edit', 'CustomerController@editCustomer');
    Route::post('customer/{id}', 'CustomerController@updateCustomer');

    Route::get('groupmember/create', 'CustomerController@createGroupMember');
    Route::post('groupmember/create', 'CustomerController@storeGroupMember');
    Route::get('groupmember/list', 'CustomerController@listGroupMembers');
    Route::get('groupmember/{id}/edit', 'CustomerController@editGroupMember');
    Route::post('groupmember/{id}', 'CustomerController@updateGroupMember');

    Route::get('reservation/create', 'CustomerController@createReservation');
    Route::post('reservation/create', 'CustomerController@storeReservation');
    Route::get('reservation/list', 'CustomerController@listReservations');
    Route::get('reservation/{id}/edit', 'CustomerController@editReservation');
    Route::post('reservation/{id}', 'CustomerController@updateReservation');

    Route::get('bill/create', 'CustomerController@createBill');
    Route::post('bill/create', 'CustomerController@storeBill');
    Route::get('bill/list', 'CustomerController@listBills');
    Route::get('bill/{id}/edit', 'CustomerController@editBill');
    Route::post('bill/{id}', 'CustomerController@updateBill');

    Route::get('room/create', 'RoomController@createRoom');
    Route::post('room/create', 'RoomController@storeRoom');
    Route::get('room/list', 'RoomController@listRooms');
    Route::get('room/{id}/edit', 'RoomController@editRoom');
    Route::post('room/{id}', 'RoomController@updateRoom');


    Route::get('facility/create', 'RoomController@createFacility');
    Route::post('facility/create', 'RoomController@storeFacility');
    Route::get('facility/list', 'RoomController@listFacilities');
    Route::get('facility/{id}/edit', 'RoomController@editFacility');
    Route::post('facility/{id}', 'RoomController@updateFacility');

    Route::get('category/create', 'RoomController@createCategory');
    Route::post('category/create', 'RoomController@storeCategory');
    Route::get('category/list', 'RoomController@listCategories');
    Route::get('category/{id}/edit', 'RoomController@editCategory');
    Route::post('category/{id}', 'RoomController@updateCategory');

    Route::get('checkedin', 'CustomerController@listCheckedIn');
    Route::get('checkedout', 'CustomerController@listCheckedOut');

});
