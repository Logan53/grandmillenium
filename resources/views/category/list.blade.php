@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-7">
  <div >
    <table class="table table-stripped">
      <thead>
        <th>ID</th>
        <th>No Of Allowed Member</th>
        <th>Price</th>
        <th>Category Name</th>
        <th>Facilities</th>
      </thead>
      <tbody>
      @foreach($categories as $category)
          <tr>
         <td>{{ $category->id }}</td>
         <td>{{ $category->noofpeople }}</td>
         <td>{{ $category->price }}</td>
         <td>{{ $category->category_name }}</td>
         <td>{{ $category->facility }}</td>
         <td class="center">
             <a class="btn btn-info" href="{{ url('category').'/'.$category->id.'/edit' }}">
                 <i class="fa fa-edit "></i>
             </a>
             <form style="display:inline" method="post" action="{{ url('category') }}/{{$category->id}}" onsubmit="return confirm('Are you sure you want to delete this category?');">
                 {!! csrf_field() !!}
                 {{ method_field('DELETE') }}
                 <button type="submit" class="btn btn-danger"><i class="fa fa-trash "></i></button>
             </form>
         </td>
          </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection