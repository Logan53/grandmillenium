@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-5">
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
    <form action="{{ url('category/create') }}" method="post">
      {{ csrf_field() }}
      <div class="form-group ">
        <label>Room Number:</label>
      <input type="text" name="roomno" class="form-control">
      </div>
      <div class="form-group">
        <label>Category Name:</label>
        <input type="text" name="name" class="form-control">
      </div>
      <div class="form-group ">
        <label>No Of Allowed Members:</label>
      <input type="text" name="noofpeople" class="form-control">
      </div>
      <div class="form-group">
        <label>Price:</label>
      <input type="text" name="price" class="form-control">
      </div>

      <div class="form-group ">
      <label>Facilities:</label>
      <select>
        <option value="">1</option>
        <option value="">2</option>
        <option value="">3</option>
        <option value="">4</option>
      </select>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" class="form-control" value="Create">
        </div>
    </form>
</div>
</div>
@endsection