@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-5">
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
    <form action="{{ url('reservation/create') }}" method="post">
      {{ csrf_field() }}
      <div class="form-group ">
      <label><span>*</span>Customer ID:</label>
      <select name="customer_id"  class="form-control">
        @foreach($customers as $customer)
        <option value="{{ $customer->id }}">{{ $customer->id }}</option>
        @endforeach
      </select>
      </div>
        <div class="form-group ">
            <label><span>*</span>Category Name:</label>
            <select name="category_name"  class="form-control">
                {{--@foreach($categories as $category)--}}
                    {{--<option value="{{ $category->name }}">{{ $category->name }}</option>--}}
                {{--@endforeach--}}
            </select>
        </div>
        <div class="form-group ">
            <label><span>*</span>Room Number:</label>
            <select name="roomno"  class="form-control">
                {{--@foreach($rooms as $room)--}}
                    {{--<option value="{{ $room->roomno }}">{{ $room->roomno }}</option>--}}
                {{--@endforeach--}}
            </select>
        </div>
        <div class="form-group ">
            <label><span>*</span>Status</label>
            <input type="text" name="status" class="form-control">
        </div>
        <div class="form-group ">
            <label><span>*</span>Floor</label>
            <input type="text" name="floor" class="form-control">
        </div>
        <div class="form-group ">
            <label><span>*</span>Location</label>
            <input type="text" name="location" class="form-control">
        </div>
        <div class="form-group">
            <label><span>*</span>Date-From:</label>
            <input type="date" name="dateFrom" class="form-control" value="">
        </div>
      <div class="form-group ">
        <label><span>*</span>Date-To:</label>
      <input type="date" name="dateTo" class="form-control" value="">
      </div>

      <div class="form-group">
        <input type="submit" class="btn btn-success" class="form-control" value="Create">
        </div>
    </form>
</div>
</div>
@endsection