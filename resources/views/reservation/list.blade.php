@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-10">
  <div >
    <table class="table table-stripped">
      <thead>
        <th>ID</th>
        <th>Customer ID</th>
        <th>Category Name</th>
        <th>Room Number</th>
        <th>Status</th>
        <th>Floor</th>
        <th>Location</th>
        <th>Date-From</th>
        <th>Date-To</th>
      </thead>
      <tbody>
        @foreach($reservations as $reservation)
            <tr>
         <td>{{ $reservation->id }}</td>
         <td>{{ $reservation->customer_id }}</td>
         <td>{{ $reservation->category_name }}</td>
         <td>{{ $reservation->roomno }}</td>
         <td>{{ $reservation->status }}</td>
         <td>{{ $reservation->floor }}</td>
         <td>{{ $reservation->location }}</td>
         <td>{{ $reservation->dateFrom }}</td>
         <td>{{ $reservation->dateTo }}</td>
         <td class="center">
             <a class="btn btn-info" href="{{ url('reservation').'/'.$reservation->id.'/edit' }}">
                 <i class="fa fa-edit "></i>
             </a>
             <form style="display:inline" method="post" action="{{ url('reservation') }}/{{$reservation->id}}" onsubmit="return confirm('Are you sure you want to delete this Reservation?');">
                 {!! csrf_field() !!}
                 {{ method_field('DELETE') }}
                 <button type="submit" class="btn btn-danger"><i class="fa fa-trash "></i></button>
             </form>
         </td>
            </tr>
            @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection