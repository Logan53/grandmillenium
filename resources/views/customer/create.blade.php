@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-5">
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
    <form action="" method="post">
      {{ csrf_field() }}
      <div class="form-group ">
        <span>*</span><label>First Name:</label>
      <input type="text" name="fname" class="form-control">
      </div>
      <div class="form-group ">
        <span>*</span><label>Last Name:</label>
      <input type="text" name="lname" class="form-control">
      </div>
      <div class="form-group">
        <span>*</span><label>CNIC:</label>
      <input type="text" name="cnic" class="form-control">
      </div>
      <div class="form-group">
        <label>Phone:</label>
      <input type="text" name="phone" class="form-control">
      </div>
      <div class="form-group">
        <label>Email:</label>
      <input type="email" name="email" class="form-control">
      </div>
      <div class="form-group">
        <label>Address:</label>
      <input type="text" name="address" class="form-control">
      </div>
      <div class="form-group">
        <label>City:</label>
      <input type="text" name="city" class="form-control">
      </div>
      <div class="form-group">
        <label>Country:</label>
      <input type="text" name="country" class="form-control">
      </div>
      <div class="form-group">
        <label>Nationality:</label>
      <input type="text" name="nationality" class="form-control">
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" class="form-control">
        </div>
    </form>
</div>
</div>
@endsection