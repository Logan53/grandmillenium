@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-9">
  <div >
    <table class="table table-stripped">
      <thead>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>CNIC</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
        <th>City</th>
        <th>Country</th>
        <th>Nationality</th>
      </thead>
      <tbody>
        @foreach($customers as $customer)
            <tr>
         <td>{{ $customer->id }}</td>
         <td>{{ $customer->fname }}</td>
         <td>{{ $customer->lname }}</td>
         <td>{{ $customer->cnic }}</td>
         <td>{{ $customer->email }}</td>
         <td>{{ $customer->phone }}</td>
         <td>{{ $customer->address }}</td>
         <td>{{ $customer->city }}</td>
         <td>{{ $customer->country }}</td>
         <td>{{ $customer->nationality }}</td>
         <td class="center">
             <a class="btn btn-info" href="{{ url('customer').'/'.$customer->id.'/edit' }}">
                 <i class="fa fa-edit "></i>
             </a>
             <form style="display:inline" method="post" action="{{ url('customer') }}/{{$customer->id}}" onsubmit="return confirm('Are you sure you want to delete this customer?');">
                 {!! csrf_field() !!}
                 {{ method_field('DELETE') }}
                 <button type="submit" class="btn btn-danger"><i class="fa fa-trash "></i></button>
             </form>
         </td>
            </tr>
            @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection