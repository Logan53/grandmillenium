@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-5">
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
<div class="form-group ">
    <form action="{{ url('customer/'.$customer->id) }}" method="post">
      {{ csrf_field() }}
      <div>
      <label>First Name:</label>
      <input type="text" name="fname" value="{{ $customer->fname }}" class="form-control">
      </div>
      <div class="form-group ">
        <label>Last Name:</label>
      <input type="text" name="lname" value="{{ $customer->lname }}" class="form-control">
      </div>
      <div class="form-group">
        <label>CNIC:</label>
      <input type="text" name="cnic" value="{{ $customer->cnic }}" class="form-control">
      </div>
      <div class="form-group">
        <label>Phone:</label>
      <input type="text" name="phone" value="{{ $customer->phone }}" class="form-control">
      </div>
      <div class="form-group">
        <label>Email:</label>
      <input type="email" name="email" value="{{ $customer->email }}" class="form-control">
      </div>
      <div class="form-group">
        <label>Address:</label>
      <input type="text" name="address" value="{{ $customer->address }}" class="form-control">
      </div>
      <div class="form-group">
        <label>City:</label>
      <input type="text" name="city" value="{{ $customer->city }}" class="form-control">
      </div>
      <div class="form-group">
        <label>Country:</label>
      <input type="text" name="country" value="{{ $customer->country }}" class="form-control">
      </div>
      <div class="form-group">
        <label>Nationality:</label>
      <input type="text" name="nationality" value="{{ $customer->nationality }}" class="form-control">
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" class="form-control" value="Update">
        </div>
    </form>
</div>
</div>
@endsection