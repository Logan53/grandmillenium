@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-5">
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
    <form action="{{ url('groupmember/create') }}" method="post">
      {{ csrf_field() }}
      <div class="form-group ">
        <span>*</span><label>Customer ID:</label>
      <input type="text" name="customer_id" class="form-control">
      </div>
      <div class="form-group ">
        <span>*</span><label>Name:</label>
      <input type="text" name="name" class="form-control">
      </div>
      <div class="form-group">
        <span>*</span><label>CNIC:</label>
      <input type="text" name="cnic" class="form-control">
      </div>
      <div class="form-group">
        <span>*</span><label>DOB:</label>
      <input type="date" name="dob" class="form-control">
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" class="form-control">
        </div>
    </form>
</div>
</div>
@endsection