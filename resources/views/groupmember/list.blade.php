@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-7">
  <div >
    <table class="table table-stripped">
      <thead>
        <th>ID</th>
        <th>Customer ID</th>
        <th>Member Name</th>
        <th>DOB</th>
        <th>CNIC</th>
       
      </thead>
      <tbody>
        @foreach($groupmembers as $groupmember)
            <tr>
         <td>{{ $groupmember->id }}</td>
         <td>{{ $groupmember->customer_id }}</td>
         <td>{{ $groupmember->name }}</td>
         <td>{{ $groupmember->dob }}</td>
         <td>{{ $groupmember->cnic }}</td>
         <td class="center">
             <a class="btn btn-info" href="{{ url('groupmember').'/'.$groupmember->id.'/edit' }}">
                 <i class="fa fa-edit "></i>
             </a>
             <form style="display:inline" method="post" action="{{ url('groupmember') }}/{{$groupmember->id}}" onsubmit="return confirm('Are you sure you want to delete this Group Member?');">
                 {!! csrf_field() !!}
                 {{ method_field('DELETE') }}
                 <button type="submit" class="btn btn-danger"><i class="fa fa-trash "></i></button>
             </form>
         </td>
            </tr>
            @endforeach
      <br>
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection