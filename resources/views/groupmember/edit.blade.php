@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-5">
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
    <form action="{{ url('groupmember/'.$groupmember->id) }}" method="post">
      {{ csrf_field() }}
      <div class="form-group ">
      <label>Customer ID:</label>
      <input type="text" name="customer_id" class="form-control" value="{{ $groupmember->customer_id }}">
      </div>
      <div class="form-group ">
        <label>Name:</label>
      <input type="text" name="name" class="form-control" value="{{ $groupmember->name }}">
      </div>
      <div class="form-group">
        <label>CNIC:</label>
      <input type="text" name="cnic" class="form-control" value="{{ $groupmember->cnic }}">
      </div>
      <div class="form-group">
        <label>DOB:</label>
      <input type="text" name="dob" class="form-control" value="{{ $groupmember->dob }}">
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" class="form-control" value="Update">
        </div>
    </form>
</div>
</div>
@endsection