@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-md-offset-1 col-md-5">
      <form action="{{ url('bill/').$bill->id }}" method="post">
        {{ csrf_field() }}
        <div class="form-group ">
          <label>Customer ID:</label>
          <input type="text" name="customer_id" class="form-control" value="{{ $bill->customer_id}}">
        </div>
        <div class="form-group ">
          <label>Room Number:</label>
          <input type="text" name="roomno" class="form-control"  value="{{ $bill->roomno}}">
        </div>
        <div class="form-group">
          <label>Items:</label>
          <input type="text" name="items" class="form-control"  value="{{ $bill->items}}">
        </div>
        <div class="form-group">
          <label>Price:</label>
          <input type="text" name="price" class="form-control"  value="{{ $bill->price}}">
        </div>
        <div class="form-group">
          <label>Room Rent:</label>
          <input type="text" name="roomrent" class="form-control"  value="{{ $bill->roomrent}}">
        </div>
        <div class="form-group">
          <label>Amount Receivable:</label>
          <input type="text" name="amountreceive" class="form-control"  value="{{ $bill->amountreceive}}">
        </div>
        <div class="form-group">
          <label>Amount Payable:</label>
          <input type="text" name="amountpayable" class="form-control"  value="{{ $bill->amountpayable}}">
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-success" class="form-control" value="Update">
        </div>
      </form>
    </div>
  </div>
@endsection