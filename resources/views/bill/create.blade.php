@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-5">
    <form action="{{ url('bill/create') }}" method="post">
      {{ csrf_field() }}
      <div class="form-group ">
      <label>Customer ID:</label>
      <input type="text" name="customer_id" class="form-control">
      </div>
      <div class="form-group ">
        <label>Room Number:</label>
      <input type="text" name="roomno" class="form-control">
      </div>
      <div class="form-group">
        <label>Items:</label>
      <input type="text" name="items" class="form-control">
      </div>
      <div class="form-group">
        <label>Price:</label>
      <input type="text" name="price" class="form-control">
      </div>
      <div class="form-group">
        <label>Room Rent:</label>
      <input type="text" name="roomrent" class="form-control">
      </div>
      <div class="form-group">
        <label>Amount Receivable:</label>
      <input type="text" name="amountreceive" class="form-control">
      </div>
      <div class="form-group">
        <label>Amount Payable:</label>
      <input type="text" name="amountpayable" class="form-control">
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" class="form-control" value="Create">
        </div>
    </form>
</div>
</div>
@endsection