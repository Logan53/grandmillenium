@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-9">
  <div >
    <table class="table table-stripped">
      <thead>
        <th>ID</th>
        <th>Customer ID</th>
        <th>Room Number</th>
        <th>Items</th>
        <th>Price</th>
        <th>Room Rent</th>
        <th>Amount Payable</th>
        <th>Amount Receivable</th>
      </thead>
      <tbody>
      @foreach($bills as $bill)
          <tr>
         <td>{{ $bill->id }}</td>
         <td>{{ $bill->customer_id }}</td>
         <td>{{ $bill->roomno }}</td>
         <td>{{ $bill->items }}</td>
         <td>{{ $bill->price }}</td>
         <td>{{ $bill->roomrent }}</td>
         <td>{{ $bill->amountreceive }}</td>
         <td>{{ $bill->amountpayable }}</td>
         <td class="center">
             <a class="btn btn-info" href="{{ url('bill').'/'.$bill->id.'/edit' }}">
                 <i class="fa fa-edit "></i>
             </a>
             <form style="display:inline" method="post" action="{{ url('bill') }}/{{$bill->id}}" onsubmit="return confirm('Are you sure you want to delete this bill?');">
                 {!! csrf_field() !!}
                 {{ method_field('DELETE') }}
                 <button type="submit" class="btn btn-danger"><i class="fa fa-trash "></i></button>
             </form>
         </td>
          </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection