@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-md-offset-1 col-md-5">
      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <form action="{{ url('room/'.$room->id) }}" method="post">
        {{ csrf_field() }}
        <div class="form-group ">
          <label><span>*</span>Room Number:</label>
          <input type="text" name="roomno" class="form-control" value="{{ $room->roomno }}">
        </div>
        <div class="form-group ">
          <label><span>*</span>Category Name:</label>
          <select name="category_name"  class="form-control">
            {{--@foreach($categories as $category)--}}
            {{--<option value="{{ $category->name }}">{{ $category->name }}</option>--}}
            {{--@endforeach--}}
          </select>
        </div>
        <div class="form-group">
          <label><span>*</span>Floor:</label>
          <input type="text" name="floor" class="form-control" value="{{ $room->floor }}">
        </div>
        <div class="form-group">
          <label><span>*</span>Location:</label>
          <input type="text" name="location" class="form-control" value="{{ $room->location }}">
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-success" class="form-control" value="Update">
        </div>
      </form>
    </div>
  </div>
@endsection