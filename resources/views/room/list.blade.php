@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-7">
  <div >
    <table class="table table-stripped">
      <thead>
        <th>ID</th>
        <th>Room Number</th>
        <th>Category Name</th>
        <th>Floor</th>
        <th>Location</th>
      </thead>
      <tbody>
        @foreach($rooms as $room)
         <td>{{ $room->id }}</td>
         <td>{{ $room->roomno }}</td>
         <td>{{ $room->category_name }}</td>
         <td>{{ $room->floor }}</td>
         <td>{{ $room->location }}</td>
         <td class="center">
             <a class="btn btn-info" href="{{ url('room').'/'.$room->id.'/edit' }}">
                 <i class="fa fa-edit "></i>
             </a>
             <form style="display:inline" method="post" action="{{ url('room') }}/{{$room->id}}" onsubmit="return confirm('Are you sure you want to delete this customer?');">
                 {!! csrf_field() !!}
                 {{ method_field('DELETE') }}
                 <button type="submit" class="btn btn-danger"><i class="fa fa-trash "></i></button>
             </form>
         </td>
            @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection