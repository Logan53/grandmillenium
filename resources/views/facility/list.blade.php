@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-7">
  <div >
    <table class="table table-stripped">
      <thead>
        <th>ID</th>
        <th>Category Name</th>
        <th>Description</th>
      </thead>
      <tbody>
      @foreach($facilities as $facility)
          <tr>
         <td>{{ $facility->id }}</td>
         <td>{{ $facility->category_name }}</td>
         <td>{{ $facility->description }}</td>
         <td class="center">
             <a class="btn btn-info" href="{{ url('facility').'/'.$facility->id.'/edit' }}">
                 <i class="fa fa-edit "></i>
             </a>
             <form style="display:inline" method="post" action="{{ url('facility') }}/{{$facility->id}}" onsubmit="return confirm('Are you sure you want to delete this facility?');">
                 {!! csrf_field() !!}
                 {{ method_field('DELETE') }}
                 <button type="submit" class="btn btn-danger"><i class="fa fa-trash "></i></button>
             </form>
         </td>
          </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
@endsection