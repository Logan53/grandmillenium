@extends('layouts.app')
@section('content')
<div class="row">
<div class="col-md-offset-1 col-md-5">
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
    <form action="{{ url('facility/'.$facility->id) }}" method="post">
      {{ csrf_field() }}
      <div class="form-group ">
      <label>Category Name:</label>
      <input type="text" name="category_name" class="form-control" value="{{ $facility->category_name }}">
      </div>
      <div class="form-group ">
        <label>Description:</label>
      <input type="text" name="description" class="form-control" value="{{ $facility->description }}">
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" class="form-control" value="Update">
        </div>
    </form>
</div>
</div>
@endsection