<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Hussnain Naveed',
            'email' => 'hussnaindar17@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        DB::table('users')->insert([
            'name' => 'Waseemm Younus',
            'email' => 'wgcu418@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
