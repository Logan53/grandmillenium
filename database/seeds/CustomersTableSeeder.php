<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'fname' => 'Hussnain Naveed',
            'lname' => 'Naveed',
            'cnic' => '1234567890123',
        ]);
    }
}
