<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'customer_id', 'dateFrom', 'dateTo', 'roomno',
    ];

    public function customer()
    {
        return $this->belongsTo('App/Customer');
    }


}
