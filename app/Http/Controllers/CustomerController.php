<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Customer;
use App\GroupMember;
use App\Reservation;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function listCustomers()
    {
        $customers = Customer::all();

        return view('customer/list', ['customers'=>$customers]);
    }

    public function createCustomer()
    {
        return view('customer/create');
    }

    public function storeCustomer(Request $request)
    {
        $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'cnic' => 'required|min:13|max:13',
        ]);

        $customer = Customer::create($request->all());

        return redirect('customer/list');
    }

    public function editCustomer($id)
    {
        $customer = Customer::find($id);

        return view('customer/edit', ['customer'=>$customer]);
    }

    public function updateCustomer(Request $request, $id)
    {
        $customer = Customer::find($id);

        $customer->fname = $request->input('fname');
        $customer->lname = $request->input('lname');
        $customer->email = $request->input('email');
        $customer->cnic = $request->input('cnic');
        $customer->phone = $request->input('phone');
        $customer->address = $request->input('address');
        $customer->city = $request->input('city');
        $customer->country = $request->input('country');
        $customer->nationality = $request->input('nationality');

        $customer->save();

        return redirect('customer/list');
    }

    public function listGroupMembers()
    {
        $groupmembers = GroupMember::all();

        return view('groupmember/list', ['groupmembers'=>$groupmembers]);
    }

    public function createGroupMember()
    {
        return view('groupmember/create');
    }

    public function storeGroupMember(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'dob' => 'required',
            'cnic' => 'required',
            'customer_id'=>'required',
        ]);

        $groupmember = GroupMember::create($request->all());

        return redirect('groupmember/list');
    }

    public function editGroupMember($id)
    {
        $groupmember = GroupMember::find($id);

        return view('groupmember/edit', ['groupmember'=>$groupmember]);
    }

    public function updateGroupMember(Request $request, $id)
    {
        $groupmember = GroupMember::find($id);


        $groupmember->name = $request->input('name');
        $groupmember->cnic = $request->input('cnic');
        $groupmember->dob = $request->input('dob');
        $groupmember->customer_id = $request->input('customer_id');

        $groupmember->save();

        return redirect('groupmember/list');
    }

    public function listReservations()
    {
        $reservations = Reservation::all();

        return view('reservation/list', ['reservations'=>$reservations]);
    }

    public function createReservation()
    {
        $customers = Customer::all();
        return view('reservation/create', ['customers'=>$customers]);
    }

    public function storeReservation(Request $request)
    {
        $this->validate($request, [
            'dateFrom' => 'required',
            'dateTo' => 'required',
        ]);

        $reservation = Reservation::create($request->all());

        return redirect('reservation/list');
    }

    public function editReservation($id)
    {
        $reservation = Reservation::find($id);
        $customers = Customer::all();
        return view('reservation/edit', ['reservation'=>$reservation, 'customers'=>$customers]);
    }

    public function updateReservation(Request $request, $id)
    {
        $reservation = Reservation::find($id);

        $reservation->dateFrom = $request->input('dateFrom');
        $reservation->dateTo = $request->input('dateTo');
        $reservation->customer_id = $request->input('customer_id');
        $reservation->category_name = $request->input('category_name');
        $reservation->status = $request->input('status');
        $reservation->roomno = $request->input('roomno');
        $reservation->floor = $request->input('floor');
        $reservation->location = $request->input('location');

        $reservation->save();

        return redirect('reservation/list');
    }

    public function listBills()
    {
        $bills = Bill::all();
        return view('bill/list', ['bills'=>$bills]);
    }

    public function createBill()
    {
        return view('bill/create');

    }

    public function storeBill(Request $request)
    {
        $this->validate($request, [
            'items' => 'required',
            'price' => 'required',
            'roomrent' => 'required',
            'amountreceive' => 'required',
            'amountpayable' => 'required',
        ]);

        $bill = Bill::create($request->all());

        return redirect('bill/list');
    }

    public function editBill($id)
    {
        $bill = Bill::find($id);

        return view('bill/edit', ['bill'=>$bill]);
    }

    public function updateBill(Request $request, $id)
    {
        $bill = Bill::find($id);


        $bill->items = $request->input('items');
        $bill->price = $request->input('price');
        $bill->roomrent = $request->input('roomrent');
        $bill->amountreceive = $request->input('amountreceive');
        $bill->amountpayable = $request->input('amountpayable');
        $bill->roomno = $request->input('roomno');
        $bill->customer_id = $request->input('customer_id');

        $bill->save();

        return redirect('bill/list');
    }


    public function listCheckedIn()
    {
        $customers = Customer::where('active', 1)->get();
        return view('checkedin', ['customers'=>$customers]);
    }

    public function listCheckedOut()
    {
        $customers = Customer::where('active', 0)->get();
        return view('checkedout', ['customers'=>$customers]);
    }
}