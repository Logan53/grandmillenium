<?php

namespace App\Http\Controllers;

use App\Category;
use App\Facility;
use App\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function listRooms()
    {
        $rooms = Room::all();

        return view('room/list', ['rooms'=>$rooms]);
    }

    public function createRoom()
    {
        $categories = Category::all();
        return view('room/create', ['categories'=>$categories]);
    }

    public function storeRoom(Request $request)
    {
        $this->validate($request, [
            'roomno' => 'required',
            'floor' => 'required',
            'location' => 'required',
        ]);

        $room = Room::create($request->all());

        return redirect('room/list');
    }

    public function editRoom($id)
    {
        $room = Room::find($id);
        $categories = Category::all();
        return view('room/edit', ['room'=>$room, 'categories'=>$categories]);
    }

    public function updateRoom(Request $request, $id)
    {
        $room = Room::find($id);

        $room->roomno = $request->input('roomno');
        $room->category_name = $request->input('category_name');
        $room->floor = $request->input('floor');
        $room->location = $request->input('location');

        $room->save();

        return redirect('room/list');
    }


    public function listCategories()
    {
        $categories = Category::all();
        return view('category/list', ['categories'=>$categories]);
    }

    public function createCategory(Request $request)
    {
        return view('category/create');
    }

    public function storeCategory(Request $request)
    {
        $category = Category::create($request->all());
        return redirect('category/list');
    }

    public function editCategory($id)
    {
        $category = Category::find($id);
        return view('category/edit', ['category'=>$category]);
    }

    public function updateCategory(Request $request, $id)
    {
        $category = Category::find($id);

        $category->roomno = $request->input('roomno');
        $category->name = $request->input('name');
        $category->noofpeople = $request->input('noofpeople');
        $category->price = $request->input('price');
        $category->facility = $request->input('facility');

        $category->save();

        return redirect('category/list');
    }


    public function listFacilities()
    {
        $facilities = Facility::all();
        return view('facility/list', ['facilities'=>$facilities]);
    }

    public function createFacility()
    {
        return view('facility/create');
    }

    public function storeFacility(Request $request)
    {
        $facility = Facility::create($request->all());
        return redirect('facility/list');
    }

    public function editFacility($id)
    {
        $facility = Facility::find($id);
        return view('facility/edit', ['facility'=>$facility]);
    }

    public function updateFacility(Request $request, $id)
    {
        $facility = Facility::find($id);

        $facility->category_name = $request->input('category_name');
        $facility->description = $request->input('description');

        $facility->save();

        return redirect('facility/list');
    }



}