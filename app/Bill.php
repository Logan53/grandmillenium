<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = [
        'items', 'price', 'roomrent', 'amountreceive','amountpayable',
    ];

    public function customer()
    {
        return $this->belongsTo('App/Customer');
    }
}
