<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    protected $fillable = [
        'name', 'dob', 'cnic','customer_id',
    ];


    public function customer()
    {
        return $this->belongsTo('App/Customer');
    }
}
