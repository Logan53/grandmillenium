<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'roomno', 'floor', 'location',
    ];

    public function customer()
    {
        return $this->belongsTo('App/Customer');
    }
}
