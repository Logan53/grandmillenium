<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'fname', 'lname', 'email', 'cnic','phone','address','city', 'country', 'nationality'
    ];

    public function groupmembers()
    {
        return $this->hasMany('App/GroupMember');
    }

    public function reservations()
    {
        return $this->hasMany('App/Reservation');
    }

    public function rooms()
    {
        return $this->hasMany('App/Room');
    }

    public function bills()
    {
        return $this->hasMany('App/Bill');
    }
}
